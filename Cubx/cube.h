#pragma once

#include <SFML/Graphics.hpp>

enum class cube_type
{
  level, player, finish
};

class cube
{
public:
  cube(unsigned size, cube_type type,
       const sf::Vector2f& velocity = sf::Vector2f(0, 0));

  cube_type get_type() const
  {
    return type_;
  };

  void set_type(cube_type type)
  {
    type_ = type;
    shape_.setFillColor(get_according_color(type_));
  }

  const sf::RectangleShape& shape() const
  {
    return shape_;
  }

  void set_position(const sf::Vector2f& position);

  void set_position(float x, float y);

  sf::Vector2f get_position() const
  {
    return shape_.getPosition();
  }

  void move_by_velocity(float multiplier = 1);

  const sf::Vector2f& get_velocity() const
  {
    return velocity_;
  }

  void set_velocity(const sf::Vector2f& velocity)
  {
    velocity_ = velocity;
  }

private:
  sf::RectangleShape shape_;
  cube_type type_;
  sf::Vector2f velocity_;

  static sf::Color get_according_color(cube_type type);
};
