#pragma once

#include <sstream>
#include "StopWatch.h"

class timer: public thor::StopWatch
{
public:
  explicit timer(bool initially_running);
  std::string get_string();
private:
    std::ostringstream string_stream_;
};
