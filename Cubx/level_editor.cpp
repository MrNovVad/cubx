#include <fstream>

#include "level_editor.h"
#include "level_manager.h"

level_editor::level_editor(std::string filename) :
window_(sf::VideoMode(600, 600), "Cubx Level Editor", sf::Style::Close),
filename_(filename),
current_type_(cube_type::level) {
  if (std::ifstream(filename_))
    // file exists, load level
    level_ = level_manager::load_level_from_file(filename_, 20, 30, 0);
}

void level_editor::start() {
  while (window_.isOpen()) {
    handle_events();
    window_.clear(sf::Color::White);
    draw_cubes();
    window_.display();
  }
}

void level_editor::handle_events() {
  sf::Event event;
  while (window_.pollEvent(event)) {
    switch (event.type) {
    case sf::Event::Closed:
      window_.close();
      break;
    case sf::Event::MouseMoved:
      handle_mouse_event();
      break;
    case sf::Event::KeyPressed:
      handle_keyboard_event();
      break;
    default:
      break;
    }
  }
}

void level_editor::draw_cubes() {
  for (auto& cube : level_.get_cubes())
    window_.draw(cube.shape());
}

void level_editor::handle_mouse_event() {
  auto cubes = level_.get_cubes();

  sf::Vector2f pos(
    (sf::Mouse::getPosition(window_).x / 20) * 20,
    (sf::Mouse::getPosition(window_).y / 20) * 20);

  auto cubeOnPos = std::find_if(cubes.begin(), cubes.end(),
                                [&pos](const cube& cube) {
    return cube.get_position() == pos;
  });
  if (cubeOnPos == cubes.end() && sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
    cube newCube(20, current_type_);
    newCube.set_position(pos);
    cubes.push_back(newCube);
  } else if (cubeOnPos != cubes.end() && sf::Mouse::isButtonPressed(sf::Mouse::Right))
    cubes.erase(cubeOnPos);

  level_.set_cubes(cubes);
}

void level_editor::handle_keyboard_event() {
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
    current_type_ = cube_type::level;
  else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
    current_type_ = cube_type::player;
  else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
    current_type_ = cube_type::finish;
  else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    level_manager::save_level_to_file(filename_, 20, 30, 0, level_);
}
