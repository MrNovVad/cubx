#include <iostream>
#include "cubx_game.h"
#include "level_editor.h"

using namespace std;

int main(int argc, char** argv)
{
  try
  {
    if (argc < 3 && argv[1] != "editor")
    {
      cubx_game game;
      game.Start();
    } else
    {
      level_editor editor(argv[2]);
      editor.start();
    }
  } catch (const std::runtime_error& re)
  {
    cerr << "Runtime error: " << re.what() << endl;
  } catch (const std::exception& ex)
  {
    cerr << "Error occurred: " << ex.what() << endl;
  } catch (...)
  {
    cerr << "Unknown failure occured. Possible memory corruption" << std::endl;
  }

  return 0;
}