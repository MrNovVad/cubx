#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Audio/Music.hpp>
#include <functional>
#include "cube.h"
#include "audio_manager.h"
#include "timer.h"
#include "message.h"

class cubx_game {
public:
  cubx_game();
  void Start();

private:
  const int CUBES_ON_SCENE = 30;
  const int CUBE_SPEED = 200;

  sf::RenderWindow window_;
  std::vector<cube> level_cubes_;
  std::vector<cube> player_cubes_;
  std::vector<cube> finish_cubes_;
  std::vector<cube> fill_cubes_;
  sf::Clock fps_clock_;
  timer timer_;
  sf::Text level_text_;
  sf::Text time_text_;
  audio_manager audio_manager_;
  sf::Music bg_music_;
  std::ostringstream time_text_stream_;
  message message_;
  sf::Font info_font_;
  unsigned const cube_size_;
  int scene_margin_;
  int current_level_;

  bool load_level(int n);
  void init_texts();
  void create_fill_cubes(const cube& from);
  void handle_key_press();
  void handle_events();
  void handle_mouse_press();
  void draw_cubes();
  void draw();
  void update(float delta);
  void update_cubes(float delta);
  void update_clock();
  void spawn_new_cube(std::vector<cube>& to, cube_type type, sf::Vector2f position,
                      sf::Vector2f velocity = sf::Vector2f(0, 0));
  void remove_cube_if(std::vector<cube>& collection, std::function<bool(const cube&)> predicate);
  bool cube_contain(const cube& cube, const sf::Vector2f point);
  bool cubes_collide(const cube& first, const cube& second);
  void spawn_cubes_around(const cube& around, std::vector<cube>& collection);
};
