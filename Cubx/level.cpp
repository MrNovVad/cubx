#include "level.h"


std::vector<cube> level::get_cubes() const
{
	return cubes_;
}

void level::set_cubes(const std::vector<cube>& cubes)
{
	cubes_ = cubes;
}

std::string level::message() const
{
	return message_;
}

level::level(const std::vector<cube>& cubes, const std::string& message)
	: cubes_(cubes), message_(message)
{
}