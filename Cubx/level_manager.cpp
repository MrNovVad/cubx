#include <iostream>
#include <fstream>
#include "level_manager.h"
#include "cube.h"

level level_manager::load_level_from_file(std::string filename,
                                          unsigned cube_size,
                                          unsigned cubes_on_scene,
                                          int margin)
{
  std::ifstream fin(filename);
  std::vector<cube> loaded_cubes;
  int x = 0, y = 0;
  cube_type type;
  std::string line, message;
  for (int i = 0; i < cubes_on_scene; ++i)
  {
    std::getline(fin, line);
    for (char cube_ch : line)
    {
      switch (cube_ch)
      {
      case CUBE_LEVEL:
        type = cube_type::level;
        break;
      case CUBE_PLAYER:
        type = cube_type::player;
        break;
      case CUBE_FINISH:
        type = cube_type::finish;
        break;
      default:
        x += cube_size;
        continue;
      }
      cube newCube(cube_size, type);
      newCube.set_position(x + margin, y);
      loaded_cubes.push_back(newCube);
      x += cube_size;
    }
    x = 0;
    y += cube_size;
  }

  std::getline(fin, message);

  return level(loaded_cubes, message);
}

void level_manager::save_level_to_file(std::string filename,
                                       unsigned cubeSize,
                                       unsigned cubesOnScene, int margin,
                                       const level& level)
{
  std::ofstream fout(filename);
  if (!fout.is_open())
    return;
  auto cubes = level.get_cubes();
  char cube_ch = '0';
  for (int i = 0; i < cubesOnScene; ++i)
  {
    int y = i * cubeSize;
    for (int j = 0; j < cubesOnScene; ++j)
    {
      int x = margin + (j * cubeSize);
      auto cube_on_pos = std::find_if(cubes.begin(), cubes.end(),
                                    [x, y](const cube& cube) {
        return cube.get_position() == sf::Vector2f(x, y);
      });

      if (cube_on_pos == cubes.end())
        cube_ch = CUBE_NONE;
      else if (cube_on_pos->get_type() == cube_type::level)
        cube_ch = CUBE_LEVEL;
      else if (cube_on_pos->get_type() == cube_type::player)
        cube_ch = CUBE_PLAYER;
      else if (cube_on_pos->get_type() == cube_type::finish)
        cube_ch = CUBE_FINISH;

      fout << cube_ch;
    }
    fout << std::endl;
    if (i == cubesOnScene - 1)
      fout << level.message();
  }
}
