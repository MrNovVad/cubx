#pragma once

#include "level.h"

namespace level_manager {
  const char CUBE_NONE = '0';
  const char CUBE_LEVEL = '1';
  const char CUBE_PLAYER = '2';
  const char CUBE_FINISH = '3';

  level load_level_from_file(std::string filename,
                             unsigned cubeSize,
                             unsigned cubesOnScene,
                             int margin);

  void save_level_to_file(std::string filename,
                          unsigned cubeSize,
                          unsigned cubesOnScene,
                          int margin,
                          const level& cubes);
}