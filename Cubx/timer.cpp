#include <iomanip>
#include "timer.h"

std::string timer::get_string()
{
    string_stream_.str("");
    string_stream_.clear();
    auto elapsed = GetElapsedTime();
    string_stream_
        << std::setfill('0') << std::setw(2)
        << int(elapsed.asSeconds()) / 60 << std::flush
        << ":"
        << std::setfill('0') << std::setw(2)
        << int(elapsed.asSeconds()) % 60 << std::flush
        << ":"
        << std::setfill('0') << std::setw(3)
        << elapsed.asMilliseconds()  % 999;
    return string_stream_.str();
}

timer::timer(bool initially_running) : StopWatch(initially_running)
{

}
