#pragma once

#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

class audio_manager {
public:
  explicit audio_manager(const std::string& audioDirectory);
  void play_spawn_sound();
  void play_new_level_sound();
  void set_volume(float volume);

private:
  sf::Sound spawn_sound_;
  sf::SoundBuffer spawn_sound_buffer_;
  sf::Sound new_level_sound_;
  sf::SoundBuffer new_level_sound_buffer_;
  const std::string directory_;
  void load_and_set_buffer(sf::Sound& sound,
                        sf::SoundBuffer& buffer,
                        const std::string& filename);

};
