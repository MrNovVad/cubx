#pragma once

#include <vector>
#include "cube.h"

class level
{
public:
  std::vector<cube> get_cubes() const;
  void set_cubes(const std::vector<cube>& cubes);
  std::string message() const;
  explicit level(const std::vector<cube>& cubes = std::vector<cube>(),
                 const std::string& message = "");

private:
	std::vector<cube> cubes_;
	std::string message_;
};
