#include "audio_manager.h"


audio_manager::audio_manager(const std::string& audioDirectory)
    : directory_(audioDirectory)
{
    load_and_set_buffer(new_level_sound_, new_level_sound_buffer_, "newlevel.ogg");
    load_and_set_buffer(spawn_sound_, spawn_sound_buffer_, "spawn.ogg");
}

void audio_manager::play_spawn_sound()
{
    spawn_sound_.play();
}

void audio_manager::play_new_level_sound()
{
    new_level_sound_.play();
}

void audio_manager::load_and_set_buffer(sf::Sound& sound,
                                    sf::SoundBuffer& buffer,
                                    const std::string& filename)
{
    buffer.loadFromFile(directory_ + "/" + filename);
    sound.setBuffer(buffer);
}

void audio_manager::set_volume(float volume)
{
    new_level_sound_.setVolume(volume);
    spawn_sound_.setVolume(volume);
}
