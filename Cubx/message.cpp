#include "message.h"

void message::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
  target.draw(static_cast<sf::RectangleShape>(*this));
  target.draw(text_);
}

message::message(const std::string& str, int window_width, int window_height, int timeout)
  : state_(hidden), str_(str), timeout_(timeout), window_height_(window_height)
{
  // load font, set color to white, set text and size
  text_font_.loadFromFile("resources/message.ttf");
  text_.setFont(text_font_);
  text_.setColor(sf::Color::White);
  text_.setString(str);
  text_.setCharacterSize(18);
  // make it opaque black and place at bottom of screen
  setFillColor(sf::Color(0, 0, 0, 120));
  setSize(sf::Vector2f(float(window_width), 40.f));
  setPosition(window_height);
}

void message::show()
{
  if (state_ != showing && state_ != visible && !str_.empty())
  {
    timeout_clock_.restart();
    state_ = showing;
  }
}

void message::update()
{
  if (timeout_clock_.getElapsedTime().asSeconds() > timeout_)
    hide();
  animate();
}

void message::set_text(const std::string& text)
{
  str_ = text;
  text_.setString(str_);
}

void message::hide()
{
  state_ = hiding;
}

void message::animate()
{
  switch (state_)
  {
  case hiding:
    if (getPosition().y == window_height_)
      state_ = hidden;
    else
      add_top(0.5f);
    break;
  case showing:
    if (getPosition().y == window_height_ - 40)
      state_ = visible;
    else
      add_top(-0.5f);
    break;
  case hidden:
  case visible:
  default: break;
  }
}

void message::add_top(float how_much)
{
  setPosition(getPosition().y + how_much);
}

void message::setPosition(float y)
{
  RectangleShape::setPosition(0, y);
  text_.setPosition(10, y + 8.f);
}