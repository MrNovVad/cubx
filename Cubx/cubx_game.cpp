#include <iostream>
#include "cubx_game.h"
#include "level_manager.h"

cubx_game::cubx_game()
  : window_(sf::VideoMode(1280, 720), "Cubx", sf::Style::Close),
  timer_(true), audio_manager_("resources"),
  message_("", window_.getSize().x, window_.getSize().y, 5),
  cube_size_(window_.getSize().y / CUBES_ON_SCENE),
  current_level_(1)
{
  bg_music_.openFromFile("resources/music.ogg");
  bg_music_.setVolume(50.f);
  bg_music_.setLoop(true);
  unsigned sceneSize = window_.getSize().y;
  scene_margin_ = (window_.getSize().x - sceneSize) / 2;
  load_level(current_level_);
  init_texts();
  bg_music_.play();
}

void cubx_game::init_texts()
{
  info_font_.loadFromFile("resources/BebasNeue Book.ttf");
  level_text_.setColor(sf::Color(255, 140, 0));
  level_text_.setPosition(20, 20);
  level_text_.setFont(info_font_);
  time_text_.setColor(sf::Color(255, 140, 0));
  time_text_.setPosition(20, 50);
  time_text_.setFont(info_font_);
}


void cubx_game::Start()
{
  while (window_.isOpen())
  {
    handle_events();
    update(fps_clock_.restart().asSeconds());
    draw();
  }
}


bool cubx_game::load_level(int n)
{

  auto level = level_manager::load_level_from_file(
    "levels/" + std::to_string(n) + ".lvl", cube_size_, CUBES_ON_SCENE, scene_margin_);

  auto cubes = level.get_cubes();

  if (cubes.empty())
    return false;

  level_cubes_.clear();
  player_cubes_.clear();
  finish_cubes_.clear();
  fill_cubes_.clear();

  for (cube& c : cubes)
  {


    if (c.get_type() == cube_type::level)
      level_cubes_.push_back(c);
    else if (c.get_type() == cube_type::player)
      player_cubes_.push_back(c);
    else if (c.get_type() == cube_type::finish)
      finish_cubes_.push_back(c);

    create_fill_cubes(c);
  }

  level_text_.setString("Level " + std::to_string(n));
  if (current_level_ != 1)
    audio_manager_.play_new_level_sound();

  message_.set_text(level.message());
  message_.hide();
  message_.show();

  return true;
}

void cubx_game::create_fill_cubes(const cube& from)
{
  if (from.get_type() == cube_type::finish || from.get_type() == cube_type::level)
  {
    bool on_left_edge = from.get_position().x == scene_margin_;
    int right_edge_pos = scene_margin_ + (cube_size_ * CUBES_ON_SCENE) - cube_size_;
    bool on_right_edge = from.get_position().x == right_edge_pos;
    if (on_left_edge)
    {
      for (int x = scene_margin_ - cube_size_; x >= -int(cube_size_); x -= cube_size_)
        spawn_new_cube(fill_cubes_, from.get_type(), sf::Vector2f(float(x), from.get_position().y));
    } else if (on_right_edge)
      for (int x = right_edge_pos; x <= int(window_.getSize().x + cube_size_); x += cube_size_)
        spawn_new_cube(fill_cubes_, from.get_type(), sf::Vector2f(float(x), from.get_position().y));
  }
}

void cubx_game::handle_events()
{
  sf::Event event;
  while (window_.pollEvent(event))
  {
    switch (event.type)
    {
    case sf::Event::KeyPressed:
      handle_key_press();
      break;
    case sf::Event::MouseButtonPressed:
      handle_mouse_press();
      break;
    case sf::Event::Closed:
      window_.close();
      break;
    default:
      break;
    }
  }
}


void cubx_game::handle_key_press()
{
  if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
    return;

  std::vector<cube> new_cubes;

  player_cubes_.erase(std::remove_if(player_cubes_.begin(), player_cubes_.end(),
    [this, &new_cubes](const cube& c) {
    spawn_cubes_around(c, new_cubes);
    return true;
  }), player_cubes_.end());

  player_cubes_.insert(player_cubes_.end(), new_cubes.begin(), new_cubes.end());
  audio_manager_.play_spawn_sound();
}

void cubx_game::draw_cubes()
{
  for (auto& cube : player_cubes_)
  {
    window_.draw(cube.shape());
  }

  for (auto& cube : level_cubes_)
  {
    window_.draw(cube.shape());
  }

  for (auto& cube : finish_cubes_)
  {
    window_.draw(cube.shape());
  }

  for (auto& cube : fill_cubes_)
  {
    window_.draw(cube.shape());
  }
}

void cubx_game::draw()
{
  window_.clear(sf::Color::White);
  draw_cubes();
  window_.draw(message_);
  window_.draw(level_text_);
  window_.draw(time_text_);
  window_.display();
}


void cubx_game::spawn_new_cube(std::vector<cube>& to, cube_type type,
                               sf::Vector2f position, sf::Vector2f velocity)
{
  cube new_cube(cube_size_, type, velocity);
  new_cube.set_position(position.x, position.y);
  to.push_back(new_cube);
}

void cubx_game::update(float delta)
{
  update_cubes(delta);
  message_.update();
  update_clock();
}


void cubx_game::update_clock()
{
  time_text_.setString(timer_.get_string());
}

void cubx_game::update_cubes(float delta)
{
  for (auto& cube : player_cubes_)
  {
    cube.move_by_velocity(delta);
  }

  // remove colliding cubes
  remove_cube_if(player_cubes_, [this](const cube& cube) {
    // delete cubes colliding with walls
    for (auto& c : level_cubes_)
      if (cubes_collide(cube, c))
        return true;
    for (auto& c : fill_cubes_)
      if (cubes_collide(cube, c))
        return true;

    // also delete cubes out of screen
    return	cube.shape().getPosition().x + cube_size_ < 0 ||
      cube.shape().getPosition().x > window_.getSize().x ||
      cube.shape().getPosition().y > window_.getSize().y ||
      cube.shape().getPosition().y + cube_size_ < 0;;
  });

  if (player_cubes_.empty())
    if (!load_level(current_level_))
      window_.close();;

  for (auto& cube1 : finish_cubes_)
  {
    for (auto& cube2 : player_cubes_)
      if (cubes_collide(cube1, cube2))
      {
        bool loaded = load_level(++current_level_);
        if (!loaded)
        {
          current_level_ = 1;
          load_level(current_level_);
        }
        return;
      }
  }
}

void cubx_game::remove_cube_if(std::vector<cube>& collection, std::function<bool(const cube&)> predicate)
{
  collection.erase(
    std::remove_if(collection.begin(), collection.end(), predicate),
    collection.end());
}

void cubx_game::handle_mouse_press()
{
  // create new vector from current mouse position relative to window
  sf::Vector2f pos(float(sf::Mouse::getPosition(window_).x), float(sf::Mouse::getPosition(window_).y));
  bool need_spawn = false;
  cube pressed(cube_size_, cube_type::player);


  remove_cube_if(player_cubes_, [&](const cube& cube) {
    if (cube_contain(cube, pos))
    {
      pressed = cube;
      need_spawn = true;
      return true;
    } else return false;
  });

  if (need_spawn)
  {
    spawn_cubes_around(pressed, player_cubes_);
    audio_manager_.play_spawn_sound();
  }
}

bool cubx_game::cube_contain(const cube& cube, const sf::Vector2f point)
{
  auto cubePos = cube.shape().getPosition();
  return point.x > cubePos.x &&
    point.x < cubePos.x + cube_size_ &&
    point.y > cubePos.y &&
    point.y < cubePos.y + cube_size_;
}

void cubx_game::spawn_cubes_around(const cube& around, std::vector<cube>& collection)
{
  spawn_new_cube(collection, cube_type::player,
                 sf::Vector2f(around.get_position().x - cube_size_,
                 around.get_position().y),
                 sf::Vector2f(-float(CUBE_SPEED), 0));
  spawn_new_cube(collection, cube_type::player,
                 sf::Vector2f(around.get_position().x + cube_size_,
                 around.get_position().y),
                 sf::Vector2f(float(CUBE_SPEED), 0));
  spawn_new_cube(collection, cube_type::player,
                 sf::Vector2f(around.get_position().x,
                 around.get_position().y + cube_size_),
                 sf::Vector2f(0, float(CUBE_SPEED)));
  spawn_new_cube(collection, cube_type::player,
                 sf::Vector2f(around.get_position().x,
                 around.get_position().y - cube_size_),
                 sf::Vector2f(0, -float(CUBE_SPEED)));
}

bool cubx_game::cubes_collide(const cube& first, const cube& second)
{
  return first.get_position().x + cube_size_ > second.get_position().x &&
    first.get_position().x < second.get_position().x + cube_size_ &&
    first.get_position().y + cube_size_ > second.get_position().y &&
    first.get_position().y < second.get_position().y + cube_size_;
}

