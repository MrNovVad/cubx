#include "cube.h"

cube::cube(unsigned size, cube_type type,
           const sf::Vector2f& velocity)
{
  set_type(type);
  shape_.setSize(sf::Vector2f(float(size), float(size)));
  velocity_ = velocity;
  type_ = type;
}

sf::Color cube::get_according_color(cube_type type)
{
  switch (type)
  {
  case cube_type::player:
    return sf::Color(4, 139, 240);
  case cube_type::finish:
    return sf::Color(254, 79, 66);
  default:
    return sf::Color(40, 40, 40);
  }
}

void cube::set_position(const sf::Vector2f& position)
{
  shape_.setPosition(position);
}

void cube::set_position(float x, float y)
{
  shape_.setPosition(x, y);
}

void cube::move_by_velocity(float multiplier)
{
  set_position(shape_.getPosition().x + velocity_.x * multiplier,
               shape_.getPosition().y + velocity_.y * multiplier);
}
