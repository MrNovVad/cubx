#pragma once

#include <SFML/Graphics.hpp>
#include "cube.h"
#include "level.h"

class level_editor {
public:
  explicit level_editor(std::string filename);
  void start();

private:
  sf::RenderWindow window_;
  std::string filename_;
  level level_;
  cube_type current_type_;

  void handle_events();
  void handle_mouse_event();
  void handle_keyboard_event();
  void draw_cubes();
};
