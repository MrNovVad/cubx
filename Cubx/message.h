#pragma once

#include <SFML/Graphics.hpp>

class message: public sf::RectangleShape
{
public:
  void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
  explicit message(const std::string& text, int window_width,
                   int window_height, int timeout = 10);
  void show();
  void hide();
  void update();
  void set_text(const std::string& text);

private:
	enum
	{
		hiding, showing, hidden, visible
	} state_;

	sf::Font text_font_;
	sf::Text text_;
	sf::Clock timeout_clock_;
	std::string str_;
	int timeout_;
	// needed for animation
	int window_height_;

	void animate();
	void add_top(float how_much);
	void setPosition(float y);
};
